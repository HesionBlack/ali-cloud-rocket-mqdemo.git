package com.aliyun.openservices.springboot.example.web;
/**
 * ClassName: ProducerController <br/>
 * Description: <br/>
 * date: 2021/11/3 11:05<br/>
 *
 * @author Hesion<br />
 * @version
 * @since JDK 1.8
 */

import com.aliyun.openservices.springboot.example.normal.RocketMessageProducer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试生产者
 * @author: hesion
 * @create: 2021-11-03 11:05
 **/
@RestController
public class ProducerController {
    /**
     * rocketmq demo
     */
    @RequestMapping(value = {"/useRocketMQ"}, method = RequestMethod.GET)
    public String useRocketMQ() {

        RocketMessageProducer.producerMsg("RocketProdTagTest","RocketProdKeyTest","RocketProdBodyTest");
        return "请求成功！";
    }
}
